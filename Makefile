
# Ex: 
#
# make PLOT  GW=S190408an TYPE=time
# make HTML  GW=S190408an TYPE=time
# make CLEAN GW=S190408an TYPE=time
#
# make HTML  GW=S190408an TYPE=event
#
# make HTML  TYPE=summary_time
# make CLEAN TYPE=summary_time
#

# gw event name -> (Ex: S190408an)
ifndef GW
GW_NAME = "all"
else
GW_NAME = ${GW}
endif

# types -> time,envelope,frequency,white,residuals,psd,skymap,event,summary
# types -> summary_time,summary_envelope,summary_frequency,summary_white,summary_residuals,summary_psd,summary_skymap
ifndef TYPE
TYPE = "all"
else
TYPE = ${PL}
endif

ifndef BATCH
BATCH = "false"
endif

ifndef PUBLIC
PUBLIC = "false"
endif

ifndef OPTS
GW_OPTS = '{"",0.0,0.0,0.0,0.0,0.0,"",0}'
else
GW_OPTS = ${OPTS}
endif

all: help

help:
	if [[ -z "${HOME_WFR}" ]]; then echo "waveform reconstructions is not initialized, do 'source setup.sh'"; exit 1; fi;
	$(HOME_WFR)/scripts/help.sh | more

# --------------------------------------------------------------------------------
# COPY
# --------------------------------------------------------------------------------

COPY:
	if [[ -z "${HOME_WFR}" ]]; then echo "waveform reconstructions is not initialized, do 'source setup.sh'"; exit 1; fi;
	# copy cWB + LALInference 90% time data files from event report directory
	if [ $(TYPE) = time ];       then $(HOME_WFR)/scripts/mkcopy.sh $(GW_NAME) time; fi;
	# copy cWB + LALInference 90% envelope data files from event report directory
	if [ $(TYPE) = envelope ];   then $(HOME_WFR)/scripts/mkcopy.sh $(GW_NAME) envelope; fi;
	# copy cWB + LALInference 90% frequency data files from event report directory
	if [ $(TYPE) = frequency ];  then $(HOME_WFR)/scripts/mkcopy.sh $(GW_NAME) frequency; fi;
	# copy cWB whitened + MaxL LALInference data files from event output directory
	if [ $(TYPE) = white ];      then 			\
	  $(HOME_WFR)/scripts/mkcopy.sh $(GW_NAME) white;	\
	  $(HOME_WFR)/scripts/mkcopy.sh $(GW_NAME) maxl_li;	\
	fi
	# copy PSD data files from event CED directory
	if [ $(TYPE) = psd ];        then $(HOME_WFR)/scripts/mkcopy.sh $(GW_NAME) psd; fi;
	# copy cWB skymap fits file from event CED directory
	if [ $(TYPE) = skymap ];     then $(HOME_WFR)/scripts/mkcopy.sh $(GW_NAME) skymap; fi;

# --------------------------------------------------------------------------------
# PLOTS
# --------------------------------------------------------------------------------

PLOT:
	if [[ -z "${HOME_WFR}" ]]; then echo "waveform reconstructions is not initialized, do 'source setup.sh'"; exit 1; fi;
	# produce cWB vs LALInference 90% time plots
	if [ $(TYPE) = time ];      then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) time plot $(GW_OPTS); fi;
	# produce cWB vs LALInference 90% envelope plots
	if [ $(TYPE) = envelope ];  then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) envelope plot $(GW_OPTS); fi;
	# produce cWB vs LALInference 90% frequency plots
	if [ $(TYPE) = frequency ]; then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) frequency plot $(GW_OPTS); fi;
	# produce cWB vs LALInference max-likelihood + whitened data time plots
	if [ $(TYPE) = white ];     then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) white plot $(GW_OPTS); fi;
	# produce residuals -> max-likelihood (cWB-LALInference) + whitened-(cWB max-likelihood) plots
	if [ $(TYPE) = residuals ]; then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) residuals plot $(GW_OPTS); fi;
	# produce strain sensitivities plots
	if [ $(TYPE) = psd ];       then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) psd plot; fi;
	# produce cWB vs LALInference sigle/combined skymaps
	if [ $(TYPE) = skymap ]; then 					\
	  if [ $(BATCH) = false ]; then					\
	    $(HOME_WFR)/scripts/mkskymap.sh $(GW_NAME) combine; 	\
	    if [ $$? -eq 1 ]; then exit 1; fi;                          \
	    $(HOME_WFR)/scripts/mkskymap.sh $(GW_NAME) single;		\
	    if [ $$? -eq 1 ]; then exit 1; fi;                          \
	  else								\
	    $(HOME_WFR)/scripts/mkskymap.sh $(GW_NAME) combine batch;	\
	    if [ $$? -eq 1 ]; then exit 1; fi;                          \
	    $(HOME_WFR)/scripts/mkskymap.sh $(GW_NAME) single  batch;	\
	    if [ $$? -eq 1 ]; then exit 1; fi;                          \
	  fi								\
	fi
	# make all plot types
	# NOTE; must be implemented -> sustitute "" with "\"
	if [ $(TYPE) = all ]; then 								\
	  make -f ${HOME_WFR}/Makefile PLOT GW=$(GW_NAME) TYPE=time       OPTS=${GW_OPTS};	\
	  make -f ${HOME_WFR}/Makefile PLOT GW=$(GW_NAME) TYPE=envelope   OPTS=${GW_OPTS};	\
	  make -f ${HOME_WFR}/Makefile PLOT GW=$(GW_NAME) TYPE=frequency  OPTS=${GW_OPTS};	\
	  make -f ${HOME_WFR}/Makefile PLOT GW=$(GW_NAME) TYPE=white      OPTS=${GW_OPTS};	\
	  make -f ${HOME_WFR}/Makefile PLOT GW=$(GW_NAME) TYPE=residuals  OPTS=${GW_OPTS};	\
	  make -f ${HOME_WFR}/Makefile PLOT GW=$(GW_NAME) TYPE=psd        OPTS=${GW_OPTS};	\
	  make -f ${HOME_WFR}/Makefile PLOT GW=$(GW_NAME) TYPE=skymap     OPTS=${GW_OPTS};	\
	fi

# --------------------------------------------------------------------------------
# HTML
# --------------------------------------------------------------------------------

HTML:
	if [[ -z "${HOME_WFR}" ]]; then echo "waveform reconstructions is not initialized, do 'source setup.sh'"; exit 1; fi;
	# produce cWB vs LALInference 90% time html page
	if [ $(TYPE) = time ];      then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) time html dummy $(PUBLIC); fi;
	# produce cWB vs LALInference 90% envelope html page
	if [ $(TYPE) = envelope ];  then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) envelope html dummy $(PUBLIC); fi;
	# produce cWB vs LALInference 90% frequency html page
	if [ $(TYPE) = frequency ]; then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) frequency html dummy $(PUBLIC); fi;
	# produce cWB vs LALInference max-likelihood + whitened data time html page
	if [ $(TYPE) = white ];     then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) white html dummy $(PUBLIC); fi;
	# produce residuals -> max-likelihood (cWB-LALInference) + whitened-(cWB max-likelihood) html page
	if [ $(TYPE) = residuals ]; then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) residuals html dummy $(PUBLIC); fi;
	# produce strain sensitivities html page
	if [ $(TYPE) = psd ];       then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) psd html dummy $(PUBLIC); fi;
	# produce cWB vs LALInference sigle/combined skymaps html page
	if [ $(TYPE) = skymap ];    then $(HOME_WFR)/scripts/mkskymap.sh $(GW_NAME) html dummy $(PUBLIC); fi;
	# produce event summary html page
	if [ $(TYPE) = event ];     then $(HOME_WFR)/scripts/mkreport.sh $(GW_NAME) event html $(GW_OPTS) $(PUBLIC); fi;
	# produce cWB summary html pages
	if [ $(TYPE) = summary_time ];      then $(HOME_WFR)/scripts/mkreport_summary.sh time $(PUBLIC); fi;	
	if [ $(TYPE) = summary_envelope ];  then $(HOME_WFR)/scripts/mkreport_summary.sh envelope $(PUBLIC); fi;	
	if [ $(TYPE) = summary_frequency ]; then $(HOME_WFR)/scripts/mkreport_summary.sh frequency $(PUBLIC); fi;	
	if [ $(TYPE) = summary_white ];     then $(HOME_WFR)/scripts/mkreport_summary.sh white $(PUBLIC); fi;	
	if [ $(TYPE) = summary_residuals ]; then $(HOME_WFR)/scripts/mkreport_summary.sh residuals $(PUBLIC); fi;	
	if [ $(TYPE) = summary_psd ];       then $(HOME_WFR)/scripts/mkreport_summary.sh psd $(PUBLIC); fi;	
	if [ $(TYPE) = summary_skymap ];    then $(HOME_WFR)/scripts/mkreport_summary.sh skymap/set1 $(PUBLIC); $(HOME_WFR)/scripts/mkreport_summary.sh skymap/set2 $(PUBLIC); fi;	
	# make all html types
	if [ $(TYPE) = all ]; then 						\
	  make -f ${HOME_WFR}/Makefile HTML  GW=$(GW_NAME) TYPE=time;		\
	  make -f ${HOME_WFR}/Makefile HTML  GW=$(GW_NAME) TYPE=envelope;	\
	  make -f ${HOME_WFR}/Makefile HTML  GW=$(GW_NAME) TYPE=frequency;	\
	  make -f ${HOME_WFR}/Makefile HTML  GW=$(GW_NAME) TYPE=white;		\
	  make -f ${HOME_WFR}/Makefile HTML  GW=$(GW_NAME) TYPE=residuals;	\
	  make -f ${HOME_WFR}/Makefile HTML  GW=$(GW_NAME) TYPE=psd;		\
	  make -f ${HOME_WFR}/Makefile HTML  GW=$(GW_NAME) TYPE=skymap;		\
	fi
	# make all html types
	if [ $(TYPE) = summary_all ]; then 					\
	  make -f ${HOME_WFR}/Makefile HTML  TYPE=summary_time;			\
	  make -f ${HOME_WFR}/Makefile HTML  TYPE=summary_envelope;		\
	  make -f ${HOME_WFR}/Makefile HTML  TYPE=summary_frequency;		\
	  make -f ${HOME_WFR}/Makefile HTML  TYPE=summary_white;		\
	  make -f ${HOME_WFR}/Makefile HTML  TYPE=summary_residuals;		\
	  make -f ${HOME_WFR}/Makefile HTML  TYPE=summary_psd;			\
	  make -f ${HOME_WFR}/Makefile HTML  TYPE=summary_skymap;		\
	fi

# --------------------------------------------------------------------------------
# CLEAN
# --------------------------------------------------------------------------------

CLEAN:
	if [[ -z "${HOME_WFR}" ]]; then echo "waveform reconstructions is not initialized, do 'source setup.sh'"; exit 1; fi;
	$(HOME_WFR)/scripts/mkclean.sh   $(GW_NAME)   $(TYPE)

