# Examples

- time plot for cWB vs LALInference 90% S190408an

```bash
     make PLOT  GW=S190408an TYPE=time
```

- html page for cWB vs LALInference 90% S190408an time plots

```bash
     make HTML  GW=S190408an TYPE=time
```

- remove html page for cWB vs LALInference 90% S190408an time plots

```bash
     make CLEAN  GW=S190408an TYPE=time
```

- make skymap plots for S190408an in batch mode

```bash
     make PLOT  GW=S190408an TYPE=time BATCH=true
```

- make skymap plots for all events in batch mode (the plots are produced in parallel)

```bash
     make PLOT  GW=all TYPE=time BATCH=true
```

- make all plots and html pages for S190408an + final event html page

```bash
     make PLOT  GW=S190408an TYPE=time
     make PLOT  GW=S190408an TYPE=envelope
     make PLOT  GW=S190408an TYPE=frequency
     make PLOT  GW=S190408an TYPE=white
     make PLOT  GW=S190408an TYPE=residuals
     make PLOT  GW=S190408an TYPE=skymap
     make PLOT  GW=S190408an TYPE=psd
```

- plot time with plot options (without white spaces)

```bash
     make PLOT  GW=S190408an TYPE=time OPTS="'{"\"GW190408_181802"\",1238782700.0,0.00,0.35,-4.0,4.0,"\"down-left"\",4}'"

     make HTML  GW=S190408an TYPE=time
     make HTML  GW=S190408an TYPE=envelope
     make HTML  GW=S190408an TYPE=frequency
     make HTML  GW=S190408an TYPE=white
     make HTML  GW=S190408an TYPE=residuals
     make HTML  GW=S190408an TYPE=skymap
     make HTML  GW=S190408an TYPE=psd
```

- final event html page: OPTS is the sub-title (optional)

```bash
     make HTML  GW=S190408an TYPE=event OPTS="'( LALInference approximant = NRSur7dq4 )'"
```

- make time plots and html pages for all events + the final summary html page:

```bash
     make PLOT  GW=all TYPE=time
     make HTML  GW=all TYPE=time
     make HTML  TYPE=summary_time
```

- remove all html event pages + summary time html page

```bash
     make CLEAN  GW=all TYPE=time
     make CLEAN  TYPE=summary_time
```

- make all types summary html pages for all events:

```bash
     make HTML  TYPE=summary_all
```

- make all types html pages for all events:

```bash
     make HTML  GW=all TYPE=all
```

- make all types plots for all events:

```bash
     make PLOT  TYPE=all
```

- O3a event data files are stored in the repository directories events/GW_NAME/data
   The O3a event data files have been copied from directories defined in the configuration file config/Makefile.cwb_pereport_config
   The commands used to load the S190408an data files into events/S190408an/data are:

```bash
     make COPY  GW=S190408an TYPE=time
     make COPY  GW=S190408an TYPE=envelope
     make COPY  GW=S190408an TYPE=frequency
     make COPY  GW=S190408an TYPE=white
     make COPY  GW=S190408an TYPE=skymap
     make COPY  GW=S190408an TYPE=psd
```

- In order to define a new event named "USER_GWNAME" not included in the O3a event list we neet to setup from bash the CWB_GWNAME environmental variable

```bash
   export CWB_GWNAME="USER_GWNAME"
```
