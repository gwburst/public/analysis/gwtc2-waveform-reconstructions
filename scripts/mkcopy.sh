#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("S190408an" "S190412m" "S190421ar" "S190503bf" "S190512at" "S190513bm" "S190517h" "S190519bj" "S190521g" "S190521r" "S190602aq" "S190701ah" "S190706ai" "S190707q" "S190727h" "S190728q" "S190814bv" "S190828j" "S190828l" "S190915ak" "$CWB_GWNAME")

if [ "$1" == '' ]; then
  echo ''
  echo 'mkcopy.sh par1 par2'
  echo ''
  echo ' par1 (optional) available options ...'
  echo ''
  echo ' default -> all'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' par2 (optional) available options: all/time/envelope/frequency/white/psd/skymap'
  echo ''
  echo ' default -> all'
  echo ''
  exit 0
fi

# check if cWB is installed
$HOME_CWB/scripts/cwb_watenv.sh
if [ $? != 0 ]; then echo ''; echo 'error: cWB must be installed !!! process terminated'; echo ''; exit 1; fi

# check if $1 is defined in the GW_LIST
CHECK='false'
for gwname in ${GW_LIST[@]} ; do
  if [ "$1" == $gwname ]; then CHECK='true'; fi;
done
if [ "$1" == 'all' ]; then CHECK='true'; fi;
if [ "$CHECK" == 'false' ]; then
  echo ''
  echo "Error: event name $1 doesn't exist in the GW_LIST"
  echo ''
  echo ' available events are:'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' in order to include the event in the list define from bash the CWB_GWNAME environmental variable'
  echo ''
  echo "  export CWB_GWNAME=$1 "
  echo ''
  exit 1
fi

DATA_TYPE='all'
if [ "$2" != '' ]; then
  DATA_TYPE=$2
fi

# copy data files
root -l -b ''$HOME_WFR'/macros/CopyEventData.C("config/Makefile.cwb_pereport_config","'$1'","'$DATA_TYPE'",".")'
if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi

exit 0

trap - INT
echo "One more time, but Ctrl-C should work again."
exit 1

