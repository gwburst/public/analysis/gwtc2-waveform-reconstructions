#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("S190408an" "S190412m" "S190421ar" "S190503bf" "S190512at" "S190513bm" "S190517h" "S190519bj" "S190521g" "S190521r" "S190602aq" "S190701ah" "S190706ai" "S190707q" "S190727h" "S190728q" "S190814bv" "S190828j" "S190828l" "S190915ak")

if [ "$1" == '' ]; then
  echo ''
  echo 'mkcopy.sh par1'
  echo ''
  echo ' par1 (optional) available options ...'
  echo ''
  echo ' default -> all'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  exit 0
fi

# check if $1 is defined in the GW_LIST
CHECK='false'
for gwname in ${GW_LIST[@]} ; do
  if [ "$1" == $gwname ]; then CHECK='true'; fi;
done
if [ "$1" == 'all' ]; then CHECK='true'; fi;
if [ "$CHECK" == 'false' ]; then
  echo ''
  echo "Error: event name $1 doesn't exist in the GW_LIST"
  echo ''
  echo ' available events are:'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' in order to include the event in the list define from bash the CWB_GWNAME environmental variable'
  echo ''
  echo "  export CWB_GWNAME=$1 "
  echo ''
  exit 1
fi

# copy O3a cwb skymap fits files
for gwname in ${GW_LIST[@]} ; do

  gwosc_name='';

  if [ "$gwname" == 'S190408an' ]; then gwosc_name='GW190408_181802';	fi;
  if [ "$gwname" == 'S190412m'  ]; then gwosc_name='GW190412';	fi;
  if [ "$gwname" == 'S190421ar' ]; then gwosc_name='GW190421_213856';	fi;
  if [ "$gwname" == 'S190503bf' ]; then gwosc_name='GW190503_185404';	fi;
  if [ "$gwname" == 'S190512at' ]; then gwosc_name='GW190512_180714';	fi;
  if [ "$gwname" == 'S190513bm' ]; then gwosc_name='GW190513_205428';	fi;
  if [ "$gwname" == 'S190517h'  ]; then gwosc_name='GW190517_055101';	fi;
  if [ "$gwname" == 'S190519bj' ]; then gwosc_name='GW190519_153544';	fi;
  if [ "$gwname" == 'S190521g'  ]; then gwosc_name='GW190521';	fi;
  if [ "$gwname" == 'S190521r'  ]; then gwosc_name='GW190521_074359';	fi;
  if [ "$gwname" == 'S190602aq' ]; then gwosc_name='GW190602_175927';	fi;
  if [ "$gwname" == 'S190701ah' ]; then gwosc_name='GW190701_203306';	fi;
  if [ "$gwname" == 'S190706ai' ]; then gwosc_name='GW190706_222641';	fi;
  if [ "$gwname" == 'S190707q'  ]; then gwosc_name='GW190707_093326';	fi;
  if [ "$gwname" == 'S190727h'  ]; then gwosc_name='GW190727_060333';	fi;
  if [ "$gwname" == 'S190728q'  ]; then gwosc_name='GW190728_064510';	fi;
  if [ "$gwname" == 'S190814bv' ]; then gwosc_name='GW190814';	fi;
  if [ "$gwname" == 'S190828j'  ]; then gwosc_name='GW190828_063405';	fi;
  if [ "$gwname" == 'S190828l'  ]; then gwosc_name='GW190828_065509';	fi;
  if [ "$gwname" == 'S190915ak' ]; then gwosc_name='GW190915_235702';	fi;


  net="HLV";
  if [ "$gwname" == 'S190421ar' ]; then net='HL';	fi;
  if [ "$gwname" == 'S190521r'  ]; then net='HL';	fi;
  if [ "$gwname" == 'S190707q'  ]; then net='HL';	fi;

  approximant="IMRPhenomPv2";
  if [ "$gwname" == 'S190412m'  ]; then approximant='IMRPhenomPv3HM';	fi;
  if [ "$gwname" == 'S190521g'  ]; then approximant='IMRPhenomPv3HM';	fi;
  if [ "$gwname" == 'S190814bv' ]; then approximant='IMRPhenomPv3HM';	fi;

  LI_SKYMAP_DIR="/home/vedovato/O3/SEARCHES/OFFLINE/BBH/LH/O3a/WFR/GWTC-2-SKYMAPS"
  if ([ $1 == $gwname ] || [ $1 == 'all' ]); then 
    cmd="cp $LI_SKYMAP_DIR//all_skymaps/$gwosc_name"_C01:"$approximant".fits" events/$gwname/data/li_skymap_$net".fits
    echo $cmd; $cmd
    if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
  fi

done

exit 0

trap - INT
echo "One more time, but Ctrl-C should work again."
exit 1

