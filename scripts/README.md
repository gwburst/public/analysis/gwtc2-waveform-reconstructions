# Scripts #

The following scripts are used to import data, generate plots and html report pages (these scripts call ROOT macros). 

* mkcopy.sh : 	imports data from the working cWB event directories (uses ROOT macro: CopyEventData.C)
* mkreport.sh :	script used to generate plots and html report pages (uses ROOT macros: DrawEventWaveforms.C, DrawEventPSD.C)
* mkskymap.sh :	script used to produce skymap plot (uses mkskymap.py)	
* mkskymap.py :	python script used by mkskymap.sh
* mkreport_summary.sh :	
* mkclean.sh : cleanup directories

* mkcopy_o3a_cwb_skymap_hlv.sh :	imports O3a cWB HLV skymaps (used only to init the git repository)	
* mkcopy_o3a_li_skymap.sh :	imports O3a LALInference skymaps (used only to init the git repository)

* help.sh :	is the help used by the main Makefile

The above scripts are called by the Makefile, to see some examples do:

```bash
  after source setup.sh do:
  make
  or 
  xmake 
```

