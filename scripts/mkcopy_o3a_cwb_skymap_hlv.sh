#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("S190408an" "S190412m" "S190421ar" "S190503bf" "S190512at" "S190513bm" "S190517h" "S190519bj" "S190521g" "S190521r" "S190602aq" "S190701ah" "S190706ai" "S190707q" "S190727h" "S190728q" "S190814bv" "S190828j" "S190828l" "S190915ak")

if [ "$1" == '' ]; then
  echo ''
  echo 'mkcopy.sh par1'
  echo ''
  echo ' par1 (optional) available options ...'
  echo ''
  echo ' default -> all'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  exit 0
fi

# check if $1 is defined in the GW_LIST
CHECK='false'
for gwname in ${GW_LIST[@]} ; do
  if [ "$1" == $gwname ]; then CHECK='true'; fi;
done
if [ "$1" == 'all' ]; then CHECK='true'; fi;
if [ "$CHECK" == 'false' ]; then
  echo ''
  echo "Error: event name $1 doesn't exist in the GW_LIST"
  echo ''
  echo ' available events are:'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' in order to include the event in the list define from bash the CWB_GWNAME environmental variable'
  echo ''
  echo "  export CWB_GWNAME=$1 "
  echo ''
  exit 1
fi

# copy O3a cwb skymap fits files
for gwname in ${GW_LIST[@]} ; do

  idir='';

  if [ "$gwname" == 'S190408an' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_run1';	   fi;
  if [ "$gwname" == 'S190412m'  ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_run1';  	   fi;
  if [ "$gwname" == 'S190503bf' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_run1';  	   fi;
  if [ "$gwname" == 'S190512at' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_S190512at_run1';  fi;
  if [ "$gwname" == 'S190513bm' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_run1';  	   fi;
  if [ "$gwname" == 'S190517h'  ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_run1';  	   fi;
  if [ "$gwname" == 'S190519bj' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_S190519bj_erun1'; fi;
  if [ "$gwname" == 'S190521g'  ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/IMBHB/LHV/EVT/O3_K99_C01_LHV_IMBHB_BKG_cwb_o3a_evt_run1';   fi;
  if [ "$gwname" == 'S190602aq' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/IMBHB/LHV/EVT/O3_K99_C01_LHV_IMBHB_BKG_cwb_o3a_evt_run1';   fi;
  if [ "$gwname" == 'S190701ah' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/IMBHB/LHV/EVT/O3_K99_C01_LHV_IMBHB_BKG_cwb_o3a_evt_erun1';  fi;
  if [ "$gwname" == 'S190706ai' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/IMBHB/LHV/EVT/O3_K99_C01_LHV_IMBHB_BKG_cwb_o3a_evt_run1';   fi;
  if [ "$gwname" == 'S190727h'  ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_run1';  	   fi;
  if [ "$gwname" == 'S190728q'  ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_run1';  	   fi;
  if [ "$gwname" == 'S190814bv' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_S190814bv_run1';  fi;
  if [ "$gwname" == 'S190828j'  ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_run1';  	   fi;
  if [ "$gwname" == 'S190828l'  ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_S190828l_run1';   fi;
  if [ "$gwname" == 'S190915ak' ]; then idir='/home/gabriele.vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/O3_K99_C01_LHV_BBH_BKG_cwb_o3a_evt_S190915ak_run1';  fi;

  if [ "$idir" != '' ] && ([ $1 == $gwname ] || [ $1 == 'all' ]); then 
    cmd="cp $idir/report/ced/$gwname/*/skyprobcc.fits events/$gwname/data/cwb_skymap_HLV.fits"
    echo $cmd; $cmd
    if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
  fi

done

exit 0

trap - INT
echo "One more time, but Ctrl-C should work again."
exit 1

