#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("S190408an" "S190412m" "S190421ar" "S190503bf" "S190512at" "S190513bm" "S190517h" "S190519bj" "S190521g" "S190521r" "S190602aq" "S190701ah" "S190706ai" "S190707q" "S190727h" "S190728q" "S190814bv" "S190828j" "S190828l" "S190915ak" "$CWB_GWNAME") 

GW_NAME=("GW190408_181802" "GW190412" "GW190421_213856" "GW190503_185404" "GW190512_180714" "GW190513_205428" "GW190517_055101" "GW190519_153544" "GW190521" "GW190521_074359" "GW190602_175927" "GW190701_203306" "GW190706_222641" "GW190707_093326" "GW190727_060333" "GW190728_064510" "GW190814" "GW190828_063405" "GW190828_065509" "GW190915_235702" "$CWB_GWNAME")

if [ "$1" == '' ]; then
  echo ''
  echo 'mkskymap.sh par1 par2 par3 par4'
  echo ''
  echo ' par1: available options are "all" or ...'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' par2: (optional) available options: single(default)/combine/html'
  echo ''
  echo ' single  -> all the fits files are converted to png with ligo-skymap-plot'
  echo ' combine -> combine cwb_hl with li and cwb_hlv cwb_li with $HOME_WFR/scripts/mkplot.py'
  echo ' html    -> produce html report'
  echo ''
  echo ''
  echo ' par3: (optional) available options: batch'
  echo ''
  echo ' default -> interactive'
  echo ''
  echo ' par4: (optional): available options: true/false(default)'
  echo ''
  echo '       if true the index.html is modified in order to be used for public pages'
  echo ''
  exit 0
fi

# check if cWB is installed
$HOME_CWB/scripts/cwb_watenv.sh
if [ $? != 0 ]; then echo ''; echo 'error: cWB must be installed !!! process terminated'; echo ''; exit 1; fi

PLOT='single'
if [ "$2" != '' ]; then
  PLOT=$2
fi

MODE=''
if [ "$3" != '' ]; then
  MODE=$3
fi

PUBLIC_INDEX='false'
if [ "$4" != '' ]; then
  PUBLIC_INDEX=$4
fi

SKYMAP50='disabled'

# check if $1 is defined in the GW_LIST
CHECK='false'
for gwname in ${GW_LIST[@]} ; do
  if [ "$1" == $gwname ]; then CHECK='true'; fi;
done
if [ "$1" == 'all' ]; then CHECK='true'; fi;
if [ "$CHECK" == 'false' ]; then
  echo ''
  echo "Error: event name $1 doesn't exist in the GW_LIST"
  echo ''
  echo ' available events are:'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' in order to include the event in the list define from bash the CWB_GWNAME environmental variable'
  echo ''
  echo "  export CWB_GWNAME=$1 "
  echo ''
  exit 1
fi

HOME_WWW=$(echo "$HOME_WWW" | sed "s/~/\/~/")
HOME_WWW=$(echo "$HOME_WWW" | sed "s/\//\\\\\//g")

# create data & report directories
for gwname in ${GW_LIST[@]} ; do

  gwid=$(echo ${GW_LIST[@]} | tr ' ' '\n' | awk '/'$gwname'/ {print NR-1}')
  GWNAME=${GW_NAME[$gwid]}

  lines=0

  li_net="HLV"
  if [ "$1" == 'all' ] || [ "$1" == $gwname ]; then

    if [ ! -d "events/$gwname/report/skymap" ]; then mkdir -p "events/$gwname/report/skymap"; fi;

    if [ "$gwname" == "S190421ar" ]; then
       li_net="HL"
    fi
    if [ "$gwname" == "S190521r" ]; then
       li_net="HL"
    fi
    if [ "$gwname" == "S190707q" ]; then
       li_net="HL"
    fi

    # combine plots
    if [ "$PLOT" == "combine" ]; then

      rm -f events/$gwname/report/skymap/cwb_li_skymap50_*.png
      rm -f events/$gwname/report/skymap/cwb_li_skymap90_*.png

      if [ "$SKYMAP50" == "enabled" ]; then
        # make combined skymap 50% with cwb HL & li_HLV
        cwb_net="HL"
        ifile_cwb="events/$gwname/data/cwb_skymap_$cwb_net"".fits"; lines=$(wc -l < $ifile_cwb)
        if [ $lines == 3 ]; then 
          echo 'error: '$ifile_cwb' is a lfs file, must be downloaded,  process terminated'; echo '';
          echo 'please do: git lfs install --skip-smudge; git lfs pull -I '$ifile_cwb; echo ''; exit 1; 
        fi
        ifile_li="events/$gwname/data/li_skymap_$li_net"".fits"; lines=$(wc -l < $ifile_li)
        if [ $lines == 3 ]; then 
          echo 'error: '$ifile_li' is a lfs file, must be downloaded,  process terminated'; echo '';
          echo 'please do: git lfs install --skip-smudge; git lfs pull -I '$ifile_li; echo ''; exit 1; 
        fi
        cmd="python $HOME_WFR/scripts/mkskymap.py map=50 $ifile_cwb $ifile_li \"cWB#($cwb_net)\" \"LI#($li_net)\" events/$gwname/report/skymap/cwb_li_skymap50_"$cwb_net"_"$li_net".png"
        if [ -e $ifile_cwb ] && [ -e $ifile_li ]; then
          if [ "$MODE" == "batch" ]; then
            echo $cmd; $cmd &
          else 
            echo $cmd; $cmd
          fi
        fi
        if [ $? != 0 ]; then echo ''; echo 'error: (check if ligo.skymap is installed) process terminated'; echo ''; exit 1; fi

        # make combined skymap 50% with cwb HLV & li_HLV
        if [ "$li_net" == "HLV" ]; then
          cwb_net="HLV"
          ifile_cwb="events/$gwname/data/cwb_skymap_$cwb_net"".fits"; lines=$(wc -l < $ifile_cwb)
          if [ $lines == 3 ]; then 
            echo 'error: '$ifile_cwb' is a lfs file, must be downloaded,  process terminated'; echo '';
            echo 'please do: git lfs install --skip-smudge; git lfs pull -I '$ifile_cwb; echo ''; exit 1; 
          fi
          ifile_li="events/$gwname/data/li_skymap_$li_net"".fits"; lines=$(wc -l < $ifile_li)
          if [ $lines == 3 ]; then 
            echo 'error: '$ifile_li' is a lfs file, must be downloaded,  process terminated'; echo '';
            echo 'please do: git lfs install --skip-smudge; git lfs pull -I '$ifile_li; echo ''; exit 1; 
          fi
          cmd="python $HOME_WFR/scripts/mkskymap.py map=50 $ifile_cwb $ifile_li \"cWB#($cwb_net)\" \"LI#($li_net)\" events/$gwname/report/skymap/cwb_li_skymap50_"$cwb_net"_"$li_net".png"
          if [ -e $ifile_cwb ] && [ -e $ifile_li ]; then
            if [ "$MODE" == "batch" ]; then
              echo $cmd; $cmd &
            else 
              echo $cmd; $cmd
            fi
          fi
          if [ $? != 0 ]; then echo ''; echo 'error: (check if ligo.skymap is installed) process terminated'; echo ''; exit 1; fi
        fi
      fi

      # make combined skymap 90% with cwb HL & li_HLV
      cwb_net="HL"
      ifile_cwb="events/$gwname/data/cwb_skymap_$cwb_net"".fits"; lines=$(wc -l < $ifile_cwb)
      if [ $lines == 3 ]; then 
        echo 'error: '$ifile_cwb' is a lfs file, must be downloaded,  process terminated'; echo '';
        echo 'please do: git lfs install --skip-smudge; git lfs pull -I '$ifile_cwb; echo ''; exit 1; 
      fi
      ifile_li="events/$gwname/data/li_skymap_$li_net"".fits"; lines=$(wc -l < $ifile_li)
      if [ $lines == 3 ]; then 
        echo 'error: '$ifile_li' is a lfs file, must be downloaded,  process terminated'; echo '';
        echo 'please do: git lfs install --skip-smudge; git lfs pull -I '$ifile_li; echo ''; exit 1; 
      fi
      cmd="python $HOME_WFR/scripts/mkskymap.py map=90 $ifile_cwb $ifile_li \"cWB#($cwb_net)\" \"LI#($li_net)\" events/$gwname/report/skymap/cwb_li_skymap90_"$cwb_net"_"$li_net".png"
      if [ -e $ifile_cwb ] && [ -e $ifile_li ]; then
        if [ "$MODE" == "batch" ]; then
          echo $cmd; $cmd &
        else 
          echo $cmd; $cmd
        fi
      fi
      if [ $? != 0 ]; then echo ''; echo 'error: (check if ligo.skymap is installed) process terminated'; echo ''; exit 1; fi

      # make combined skymap 90% with cwb HLV & li_HLV
      if [ "$li_net" == "HLV" ]; then
        cwb_net="HLV"
        ifile_cwb="events/$gwname/data/cwb_skymap_$cwb_net"".fits"; lines=$(wc -l < $ifile_cwb)
        if [ $lines == 3 ]; then 
          echo 'error: '$ifile_cwb' is a lfs file, must be downloaded,  process terminated'; echo '';
          echo 'please do: git lfs install --skip-smudge; git lfs pull -I '$ifile_cwb; echo ''; exit 1; 
        fi
        ifile_li="events/$gwname/data/li_skymap_$li_net"".fits"; lines=$(wc -l < $ifile_li)
        if [ $lines == 3 ]; then 
          echo 'error: '$ifile_li' is a lfs file, must be downloaded,  process terminated'; echo '';
          echo 'please do: git lfs install --skip-smudge; git lfs pull -I '$ifile_li; echo ''; exit 1; 
        fi
        cmd="python $HOME_WFR/scripts/mkskymap.py map=90 $ifile_cwb $ifile_li \"cWB#($cwb_net)\" \"LI#($li_net)\" events/$gwname/report/skymap/cwb_li_skymap90_"$cwb_net"_"$li_net".png"
        if [ -e $ifile_cwb ] && [ -e $ifile_li ]; then
          if [ "$MODE" == "batch" ]; then
            echo $cmd; $cmd &
          else 
            echo $cmd; $cmd
          fi
        fi
        if [ $? != 0 ]; then echo ''; echo 'error: (check if ligo.skymap is installed) process terminated'; echo ''; exit 1; fi
      fi

    fi

    # single plots
    if [ "$PLOT" == "single" ]; then

      rm -f events/$gwname/report/skymap/li_skymap_*.png
      rm -f events/$gwname/report/skymap/cwb_skymap_*.png

      for f in events/"$gwname"/data/*.fits;  do 
        ofile=$(echo "${f}" | sed "s/.fits/.png/") 
        ofile=$(echo "${ofile}" | sed "s/data/report\/skymap/") 
        lines=$(wc -l < ${f})
        if [ $lines == 3 ]; then 
          echo 'error: '$ifile_li' is a lfs file, must be downloaded,  process terminated'; echo '';
          echo 'please do: git lfs install --skip-smudge; git lfs pull -I '${f}; echo ''; exit 1; 
        fi
        cmd="ligo-skymap-plot "${f}" -o "$ofile" --contour 50 90 --annotate --figure-height 4"
        if [ "$MODE" == "batch" ]; then
          echo $cmd; $cmd &
        else 
          echo $cmd; $cmd
        fi
        if [ $? != 0 ]; then echo ''; echo 'error: (check if ligo.skymap is installed) process terminated'; echo ''; exit 1; fi
      done
    fi

    # produce html report
    if [ "$PLOT" == "html" ]; then
      # make html page
      if [ "$SKYMAP50" == "enabled" ]; then
        $HOME_CWB/scripts/cwb_mkhtml.csh "events/"$gwname"/report/skymap/" '--title '$GWNAME' --subtitle skymap:#cWB#vs#LALInference --multi true'
      else
        $HOME_CWB/scripts/cwb_mkhtml.csh "events/"$gwname"/report/skymap/" '--title '$GWNAME' --subtitle skymap:#cWB#vs#LALInference'
      fi
      sed -i 's/width=\"1000\"/width=\"900\"/'  events/"$gwname"/report/skymap/png_html_index/index.html

      COUNTER=1
      # we sorted to be compatible with the sorting used by the cwb_mkhtml command 
      for f in $(ls -1 events/"$gwname"/report/skymap/*.png | sort --ignore-case) ; do
        ofile=$(echo "${f}" | sed "s/.fits/.png/") 
        ofile=$(echo "${ofile}" | sed "s/data/report\/skymap/") 
        label="${ofile##*/}"
        if [ "$label" == "cwb_li_skymap50_HL_HL.png"   ]; then label="Skymap 50% - cWB (HL) vs LALInference (HL)";   fi
        if [ "$label" == "cwb_li_skymap50_HV_HV.png"   ]; then label="Skymap 50% - cWB (HV) vs LALInference (HV)";   fi
        if [ "$label" == "cwb_li_skymap50_LV_LV.png"   ]; then label="Skymap 50% - cWB (LV) vs LALInference (LV)";   fi
        if [ "$label" == "cwb_li_skymap50_HL_HLV.png"  ]; then label="Skymap 50% - cWB (HL) vs LALInference (HLV)";  fi
        if [ "$label" == "cwb_li_skymap50_HLV_HL.png"  ]; then label="Skymap 50% - cWB (HLiV) vs LALInference (HL)"; fi
        if [ "$label" == "cwb_li_skymap50_HLV_HLV.png" ]; then label="Skymap 50% - cWB (HLV) vs LALInference (HLV)"; fi
        if [ "$label" == "cwb_li_skymap90_HL_HL.png"   ]; then label="Skymap 90% - cWB (HL) vs LALInference (HL)";   fi
        if [ "$label" == "cwb_li_skymap90_HV_HV.png"   ]; then label="Skymap 90% - cWB (HV) vs LALInference (HV)";   fi
        if [ "$label" == "cwb_li_skymap90_LV_LV.png"   ]; then label="Skymap 90% - cWB (LV) vs LALInference (LV)";   fi
        if [ "$label" == "cwb_li_skymap90_HL_HLV.png"  ]; then label="Skymap 90% - cWB (HL) vs LALInference (HLV)";  fi
        if [ "$label" == "cwb_li_skymap90_HLV_HL.png"  ]; then label="Skymap 90% - cWB (HLiV) vs LALInference (HL)"; fi
        if [ "$label" == "cwb_li_skymap90_HLV_HLV.png" ]; then label="Skymap 90% - cWB (HLV) vs LALInference (HLV)"; fi
        if [ "$label" == "cwb_skymap_HL.png"           ]; then label="cWB (HL)";                        fi
        if [ "$label" == "cwb_skymap_HV.png"           ]; then label="cWB (HV)";                        fi
        if [ "$label" == "cwb_skymap_LV.png"           ]; then label="cWB (LV)";                        fi
        if [ "$label" == "cwb_skymap_HLV.png"          ]; then label="cWB (HLV)";                       fi
        if [ "$label" == "li_skymap_HL.png"            ]; then label="LALInference (HL)";               fi
        if [ "$label" == "li_skymap_HV.png"            ]; then label="LALInference (HV)";               fi
        if [ "$label" == "li_skymap_LV.png"            ]; then label="LALInference (LV)";               fi
        if [ "$label" == "li_skymap_HLV.png"           ]; then label="LALInference (HLV)";              fi
        sed -i 's/<h2>'"$COUNTER"'<\/h2>/<h2>'"$label"'<\/h2>/'  events/"$gwname"/report/skymap/png_html_index/index.html
        COUNTER=$[$COUNTER +1]
      done

      if [ $PUBLIC_INDEX == 'true' ]; then
        # the index.html is modified in order to be used for public pages
        ln -sf ../../../html  -t  events/"$gwname"/report/
        ln -sf ../../html  -t  events/"$gwname"/
        sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/html\//g' events/"$gwname"/report/$TYPE/png_html_index/index.html
      fi
    fi
  fi

done

exit 0

trap - INT
echo "One more time, but Ctrl-C should work again."
exit 1

